import numpy as np
from pycuda import gpuarray, cumath, autoinit
from .scattering_utilities import compute_spherical_harmonic_wavelet_normalizing_constant, factorial

#@profile
def compute_associated_legendre_polynomial_on_gpu_grid(gpu_grid, l, m, output_gpu_array, condon_shortley_phase=True):
    """
    Parameters:
    ==========
    Returns:
    """

    assert np.int(m) == m and np.int(l) == l, "m and l must be integers"
    assert np.abs(m) <= l, "m must be between -l and +l, got (l,m) = {}".format((l, m))

    if l == 0:
        output_gpu_array.fill(1.)
    elif l == 1:
        if m == -1:
            output_gpu_array[:] = 0.5 * cumath.sqrt(1 - gpu_grid**2)
        elif m == 0:
            output_gpu_array[:] = gpu_grid
        elif m == 1:
            output_gpu_array[:] = -cumath.sqrt(1 - gpu_grid**2)
    elif l == 2:
        if m == -2:
            output_gpu_array[:] = 1. / 8 * (1 - gpu_grid**2)
        elif m == -1:
            output_gpu_array[:] = 0.5 * gpu_grid * cumath.sqrt(1 - gpu_grid**2)
        elif m == 0:
            output_gpu_array[:] = 0.5 * (3 * gpu_grid**2 - 1)
        elif m == 1:
            output_gpu_array[:] = -3 * gpu_grid * cumath.sqrt(1 - gpu_grid**2)
        elif m == 2:
            output_gpu_array[:] = 3 * (1 - gpu_grid**2)
    elif l == 3:
        if m == -3:
            output_gpu_array[:] = 15. / 720 * cumath.sqrt((1 - gpu_grid**2)**3)
        elif m == -2:
            output_gpu_array[:] = 15. / 120 * gpu_grid * (1 - gpu_grid**2)
        elif m == -1:
            output_gpu_array[:] = 1. / 8 * (5 * gpu_grid**2 - 1) * cumath.sqrt(1 - gpu_grid**2)
        elif m == 0:
            output_gpu_array[:] = 0.5 * (5 * gpu_grid**3 - 3 * gpu_grid)
        elif m == 1:
            output_gpu_array[:] = -1.5 * (5 * gpu_grid**2 - 1) * cumath.sqrt(1 - gpu_grid**2)
        elif m == 2:
            output_gpu_array[:] = 15. * gpu_grid * (1 - gpu_grid**2)
        elif m == 3:
            output_gpu_array[:] = -15. * cumath.sqrt((1 - gpu_grid**2)**3)
    elif l == 4:
        if m == -4:
            output_gpu_array[:] = 105 / 40320 * (1 - gpu_grid**2)**2
        elif m == -3:
            output_gpu_array[:] = 105 / 5040 * gpu_grid * cumath.sqrt((1 - gpu_grid**2)**3)
        elif m == -2:
            output_gpu_array[:] = 15. / 720 * (7 * gpu_grid**2 - 1) * (1 - gpu_grid**2)
        elif m == -1:
            output_gpu_array[:] = 1. / 8 * (7 * gpu_grid**3 - 3 * gpu_grid) * cumath.sqrt(1 - gpu_grid**2)
        elif m == 0:
            output_gpu_array[:] = 1. / 8 * (35 * gpu_grid**4 - 30 * gpu_grid**2 + 3)
        elif m == 1:
            output_gpu_array[:] = -5. / 2 * (7 * gpu_grid**3 - 3 * gpu_grid) * cumath.sqrt(1 - gpu_grid**2)
        elif m == 2:
            output_gpu_array[:] = 15. / 2 * (7 * gpu_grid**2 - 1) * (1 - gpu_grid**2)
        elif m == 3:
            output_gpu_array[:] = -105 * gpu_grid * cumath.sqrt((1 - gpu_grid**2)**3)
        elif m == 4:
            output_gpu_array[:] = 105 * (1 - gpu_grid**2)**2
    else:
        raise NotImplementedError("Associated legendre polynomials implemented only for l <= 4, got l = {}".format(l))

    if not condon_shortley_phase and m % 2 == 1:
        output_gpu_array *= -1

def compute_spherical_harmonic_on_gpu_grid(gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array, gpu_grid_cos_polar_angle=None, computations_array=None):
    """
    Parameters:
    ==========
    Returns:
    """

    if computations_array is None:
        computations_array = gpuarray.zeros(output_gpu_array.shape, dtype='float32')

    C_l_m = np.sqrt((2 * l + 1) * factorial(l - m) / (4 * np.pi * factorial(l + m)))

    if gpu_grid_cos_polar_angle is not None:
        compute_associated_legendre_polynomial_on_gpu_grid(gpu_grid_cos_polar_angle, l, m, computations_array)
    else:
        compute_associated_legendre_polynomial_on_gpu_grid(cumath.cos(gpu_grid_polar_angle), l, m, computations_array)

    output_gpu_array.fill(C_l_m)
    output_gpu_array *= computations_array.astype('complex64') * cumath.exp(np.complex64(m * 1j) * gpu_grid_azimuthal_angle.astype('complex64'))

def compute_solid_harmonic_on_gpu_grid(gpu_grid_radius, gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array, gpu_grid_cos_polar_angle=None):
    """
    Parameters:
    ==========
    Returns:
    """
    if gpu_grid_cos_polar_angle is not None:
        compute_spherical_harmonic_on_gpu_grid(gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array, gpu_grid_cos_polar_angle=gpu_grid_cos_polar_angle)
    else:
        compute_spherical_harmonic_on_gpu_grid(gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array)

    output_gpu_array *= (gpu_grid_radius**l).astype('complex64')

def compute_solid_harmonic_wavelet_on_gpu_grid(gpu_grid_radius, gpu_grid_polar_angle, gpu_grid_azimuthal_angle, wavelet_sigma, l, m, output_gpu_array, gpu_grid_cos_polar_angle=None):
    """
    Parameters:
    ==========
    Returns:
    """

    if gpu_grid_cos_polar_angle is not None:
        compute_solid_harmonic_on_gpu_grid(gpu_grid_radius, gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array, gpu_grid_cos_polar_angle=gpu_grid_cos_polar_angle)
    else:
        compute_solid_harmonic_on_gpu_grid(gpu_grid_radius, gpu_grid_polar_angle, gpu_grid_azimuthal_angle, l, m, output_gpu_array)

    normalizing_constant = compute_spherical_harmonic_wavelet_normalizing_constant(l) / (np.power(2 * np.pi, 1.5) * wavelet_sigma**(l+3))
    output_gpu_array *= normalizing_constant
    output_gpu_array *= (cumath.exp(-gpu_grid_radius**2 / (2 * wavelet_sigma**2))).astype('complex64')
