from .scattering_utilities import determine_lowest_distance_and_bounding_box, compute_zero_order_scattering_coefficients, get_first_order_density_sampling_parameters, compute_aliasing_estimate
from .wavelets import solid_harmonics
from .cpu_density import generate_first_order_space_density, get_real_and_complex_data_types, compute_second_order_density
from multiprocessing import Pool

import sys
import time
import numpy as np

def compute_spherical_harmonic_scattering_transform(positions, properties, maximum_scale=4, maximum_n_oscillations=3, n_exponents=2, aliasing_precision=1e-3, overlapping_precision=1e-1, order_1_only=True, single_precision=True, n_jobs=1):

    lowest_distance, bbox, min_pos, max_pos = determine_lowest_distance_and_bounding_box(positions, return_min_and_max_positions=True)

    n_positions = positions.shape[0]
    property_size = properties[0].shape[1]

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    J = maximum_scale
    L = maximum_n_oscillations

    zero_order_scattering_coefficients = compute_zero_order_scattering_coefficients(properties)
    first_order_scattering_coefficients = np.zeros((n_positions, property_size, L, J+1, n_exponents))
    second_order_scattering_coefficients = np.zeros((n_positions, property_size, L, J+1, L, J+1, n_exponents))

    batch_size = np.ceil(n_positions / n_jobs).astype('int')

    print("Dividing data in {} batches of size {}".format(n_jobs, batch_size))

    positions_batches = [positions[i_job * batch_size: min((i_job+1) * batch_size, n_positions)] for i_job in range(n_jobs)]
    properties_batches = [properties[i_job * batch_size: min((i_job+1) * batch_size, n_positions)] for i_job in range(n_jobs)]

    for l_1 in range(1, L+1):
        for j_1 in range(0, J+1):
            gaussian_width, delta, space_grid = get_first_order_density_sampling_parameters(lowest_distance, min_pos, max_pos, l_1, j_1, aliasing_precision, overlapping_precision, J, L, order_1_only=order_1_only)
            scale_factor = delta / lowest_distance
            nx, ny, nz = space_grid.shape[1:]
            wavelet_sigma = gaussian_width
            print("l_1 = {}, j_1 = {}, grid dim: {} x {} x {}".format(l_1, j_1, nx, ny, nz))
            t_0 = time.time()

            if not order_1_only:
                sigmas = wavelet_sigma * 2**np.arange(j_1+1, J+1)
                wavelet_ls = np.arange(1, L+1)
                fourier_grid = np.mgrid[-np.pi:np.pi:1j*nx,-np.pi:np.pi:1j*ny,-np.pi:np.pi:1j*nz]
                fourier_wavelets = solid_harmonics(fourier_grid, sigmas, wavelet_ls)
            else:
                fourier_wavelets = None

            pool = Pool(n_jobs)
            results = []

            for i_batch, (positions_batch, properties_batch) in enumerate(zip(positions_batches, properties_batches)):
                scaled_positions_batch = positions_batch * scale_factor
                args = (
                    scaled_positions_batch,
                    properties_batch,
                    space_grid,
                    gaussian_width,
                    wavelet_sigma,
                    l_1,
                    j_1,
                    L,
                    J,
                    n_exponents,
                    aliasing_precision
                )
                keyword_args = dict(
                    fourier_wavelets=fourier_wavelets,
                    order_1_only=order_1_only,
                    single_precision=single_precision,
                    batch_index = i_batch
                )

                results.append(pool.apply_async(compute_scattering_coefficients, args, keyword_args))

            for i_batch,result in enumerate(results):
                start_index = i_batch * batch_size
                end_index = min((i_batch + 1) * batch_size, n_positions)
                first_order_scattering_coefficients[start_index:end_index, :, l_1-1, j_1, :], second_order_scattering_coefficients[start_index:end_index, :, l_1-1, j_1, :, :, :] = result.get()

            pool.close()
            pool.join()
            t_1 = time.time()
            print("done in {} sec".format(int(t_1 - t_0)))

    if order_1_only:
        return zero_order_scattering_coefficients, first_order_scattering_coefficients
    else:
        return zero_order_scattering_coefficients, first_order_scattering_coefficients, second_order_scattering_coefficients

def compute_scattering_coefficients(positions, properties, space_grid, gaussian_width, wavelet_sigma, wavelet_l, wavelet_j, L, J, n_exponents, aliasing_precision, fourier_wavelets=None, order_1_only=True, single_precision=True, batch_index=None):
    n_positions = positions.shape[0]
    property_size = properties[0].shape[1]

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    first_order_scattering_coefficients = np.ones((n_positions, property_size, n_exponents))
    second_order_scattering_coefficients = np.zeros((n_positions, property_size, L, J+1, n_exponents))

    for i_position in range(n_positions):
        centers = positions[i_position]
        props = properties[i_position]
        first_order_density = generate_first_order_space_density(centers, props, space_grid, gaussian_width, wavelet_sigma, wavelet_l, wavelet_j, single_precision=single_precision)
        if order_1_only:
            first_order_scattering_coefficients[i_position, :, 0] = np.sum(first_order_density, axis=(1,2,3))
            first_order_scattering_coefficients[i_position, :, 1] = np.sum(first_order_density**2, axis=(1,2,3))
        else:
            fourier_first_order_density = np.fft.fftn(first_order_density, axes=(1,2,3))
            aliasing = compute_aliasing_estimate(fourier_first_order_density)
            if aliasing > aliasing_precision:
                raise Warning("batch {}, system {}, aliasing {} > aliasing_precision {}".format(batch_index, i_position, aliasing, aliasing_precision))
            first_order_scattering_coefficients[i_position, :, 0] = fourier_first_order_density[:,0,0,0]
            first_order_scattering_coefficients[i_position, :, 1] = np.sum(first_order_density**2, axis=(1,2,3))

            end_index = 0
            for l_2 in range(1, L+1):
                for j_2 in range(1, J+1):
                    print("l_2 = {}, j_2 = {}".format(l_2, j_2))
                    start_index = end_index
                    end_index += 2 * l_2 + 1
                    fourier_wavelets_l = fourier_wavelets[j_2, start_index:end_index]
                    second_order_density, second_order_density_square = compute_second_order_density(fourier_first_order_density, fourier_wavelets_l, return_square=True)
                    coeff_q_1 = np.sum(second_order_density, axis=(1,2,3))
                    coeff_q_2 = np.sum(second_order_density_square, axis=(1,2,3))
                    second_order_scattering_coefficients[i_position, :, l_2-1, j_2, 0] = coeff_q_1
                    second_order_scattering_coefficients[i_position, :, l_2-1, j_2, 1] = coeff_q_2

    return first_order_scattering_coefficients, second_order_scattering_coefficients
