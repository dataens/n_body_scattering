import numpy as np
from scipy.spatial.distance import pdist
from scipy.optimize import fsolve
from numpy import log, power
from scipy.integrate import quad


def get_real_and_complex_data_types(single_precision):
    real_dtype = 'float32' if single_precision else 'float64'
    complex_dtype = 'complex64' if single_precision else 'complex128'

    return real_dtype, complex_dtype

def translate_positions(positions, translation):
    """
    Parameters
    ==========
    Returns:
    """
    translated_positions = []
    for i in range(positions.shape[0]):
        translated_positions.append(positions[i].copy())
        for j in range(positions[i].shape[0]):
            translated_positions[i][j] += translation

    return np.array(translated_positions)





def determine_lowest_distance_and_bounding_box(positions, charges, return_min_and_max_positions=False):
    """
    Parameters
        positions: 1D array of n_sytems objects
            objects are 2D array of size n_bodies x dimension
    =======
    Returns
        lowest_distance: float
            lowest distance between two bodies
        bounding_box: 1D array with 3 elements
            maximal dimension in each direction
    =======
    """
    lowest_distance = np.inf
    bounding_box = [-np.inf, -np.inf, -np.inf]
    min_positions = [np.inf, np.inf, np.inf]
    max_positions = [-np.inf, -np.inf, -np.inf]

    for i in range(len(positions)):
        body_positions = positions[i]
        body_positions = body_positions [charges[i].sum(1)>0]

        distances = pdist(body_positions)
        n_body_system_lowest_distance = np.min(distances)
        if n_body_system_lowest_distance < lowest_distance:
            lowest_distance = n_body_system_lowest_distance

        for (j, max_l) in enumerate(bounding_box):
            l = np.max(body_positions[:, j]) - np.min(body_positions[:, j])
            if l > max_l:
                bounding_box[j] = l

        for (j, min_pos) in enumerate(min_positions):
            min_position = np.min(body_positions[:, j])
            if min_position < min_pos:
                min_positions[j] = min_position

        for (j, max_pos) in enumerate(max_positions):
            max_position = np.max(body_positions[:, j])
            if max_position > max_pos:
                max_positions[j] = max_position

    if return_min_and_max_positions:
        return lowest_distance, np.array(bounding_box), np.array(min_positions), np.array(max_positions)
    else:
        return lowest_distance, np.array(bounding_box)

def compute_sampling_parameters(minimal_distance, bounding_box, maximum_wavelet_diameter, aliasing_precision=1e-6, overlapping_precision=1e-1,
        max_downsampling=2):
    """
    Parameters
        lowest_distance: float
            lowest distance between two bodies

        bounding_box: 1D array with 3 elements
            maximal dimension in each direction

        aliasing_precision: float

        overlapping_precision: float

        maximum_wavelet_diameter: float
            Maximal diameter of the wavelets defined in the original unit
    ==========
    Returns
    =======
    """
    assert aliasing_precision < 1, 'aliasing_precision must be smaller than 1'
    assert overlapping_precision < 1, 'overlapping_precision must be smaller than 1'

    sigma = np.sqrt(2 * log(1. / aliasing_precision)) / np.pi
    delta = sigma * np.sqrt(-8 * log(overlapping_precision))
    box = (bounding_box + maximum_wavelet_diameter) * delta / minimal_distance + delta

    return np.ceil(box/2**max_downsampling)*2**max_downsampling, delta, sigma

def compute_first_order_density_bounding_box(minimal_distance, min_positions, max_positions, sigma, aliasing_precision, overlapping_precision, wavelet_sigma, wavelet_l, wavelet_j):

    assert aliasing_precision < 1, 'aliasing_precision must be smaller than 1'
    assert overlapping_precision < 1, 'overlapping_precision must be smaller than 1'

    delta = sigma * np.sqrt(-8 * log(overlapping_precision))
    scale_factor = delta / minimal_distance
    sigma_Psi = np.sqrt(sigma**2 + (wavelet_sigma * 2**wavelet_j)**2)
    Psi_function_diameter = compute_spherical_harmonic_wavelet_diameter(sigma_Psi, wavelet_l, precision=0.5*aliasing_precision)
    min_indices = []
    max_indices = []
    for min_pos in min_positions:
        min_indices.append(np.floor(min_pos * scale_factor - 0.5 * Psi_function_diameter))
    for max_pos in max_positions:
        max_indices.append(np.ceil(max_pos * scale_factor + 0.5 * Psi_function_diameter))

    return delta, np.array(min_indices), np.array(max_indices)

def get_first_order_density_sampling_parameters(lowest_distance, min_positions, max_positions, wavelet_l, wavelet_j, aliasing_precision, overlapping_precision, maximum_wavelet_j, maximum_wavelet_l, order_1_only=False):
    sigma = get_first_order_density_sigma(wavelet_l, wavelet_j, aliasing_precision, overlapping_precision)
    wavelet_sigma = sigma
    delta = sigma * np.sqrt(-8 * log(overlapping_precision))
    psi_sigma = np.sqrt(sigma**2  + (wavelet_sigma * 2**wavelet_j)**2)
    padding_size = 0.5 * compute_spherical_harmonic_wavelet_diameter(psi_sigma, wavelet_l, precision=aliasing_precision)

    if not order_1_only:
        maximum_wavelet_sigma = wavelet_sigma * 2**maximum_wavelet_j
        padding_size += 0.5 * compute_spherical_harmonic_wavelet_diameter(maximum_wavelet_sigma, maximum_wavelet_l, precision=overlapping_precision)

    scale_factor = delta / lowest_distance
    print(sigma, delta, padding_size)

    x_min, y_min, z_min = np.floor(min_positions * scale_factor - padding_size)
    x_max, y_max, z_max = np.ceil(max_positions * scale_factor + padding_size)

    space_grid = np.mgrid[x_min:x_max,y_min:y_max,z_min:z_max]

    return sigma, delta, space_grid

def get_first_order_density_sigma(wavelet_l, wavelet_j, aliasing_precision, overlapping_precision):
    if aliasing_precision == 1e-3 and overlapping_precision == 1e-1:
        #numerically computed values
        sigmas_1e_3 = np.array([
            [ 2.17255652, 2.17255652, 1.60325403, 0.72758478, 0.39622887],
            [ 1.70371163, 1.70371163, 0.87310174, 0.44743878, 0.14984634],
            [ 1.92390484, 1.92390484, 1.04772209, 0.47547465, 0.20305556]
        ])
        return sigmas_1e_3[wavelet_l-1, wavelet_j]
    else:
        raise NotImplementedError("only value possible : aliasing_precision 1e-3, overlapping_precision 1e-1")


def compute_spherical_harmonic_wavelet_diameter(wavelet_sigma, n_oscillations, precision=0.1):
    assert precision >=0 and precision <= 1, "precision must be > 0 and < 1, got {}".format(precision)

    l = n_oscillations
    if l % 2 == 0:
        integral = double_factorial(l - 1) * np.sqrt(np.pi / 2)
    else:
        integral = factorial((l - 1) / 2) * power(2, (l - 1) / 2)

    exp_time_monom = lambda r: np.exp(-0.5 * r**2) * r**l
    intergral_exp_time_monom = lambda r: quad(exp_time_monom, 0, r)
    func = lambda r: intergral_exp_time_monom(r) - (1 - precision) * integral

    initial_guess = np.sqrt(l)
    diameter = 2 * wavelet_sigma * fsolve(func, initial_guess)[0]

    return diameter

def compute_zero_order_scattering_coefficients(properties):
    n_properties = properties.shape[0]
    property_size = properties[0].shape[1]
    coefficients = np.zeros((n_properties, property_size))

    for i in range(n_properties):
        coefficients[i, :] = np.sum(properties[i], axis=0)

    return coefficients

def compute_aliasing_estimate(fourier_function, fftshifted=False):
    nx, ny, nz = fourier_function.shape

    if fftshifted:
        modulus_fourier_function = np.abs(np.fft.ifftshift(fourier_function))
    else:
        modulus_fourier_function = np.abs(fourier_function)

    max_boudary_value = max(
        np.max(modulus_fourier_function[nx//2, :, :]),
        np.max(modulus_fourier_function[:, ny//2, :]),
        np.max(modulus_fourier_function[:, :, nz//2])
    )

    return max_boudary_value / modulus_fourier_function[0, 0, 0]

def double_factorial(n):
    return (1 if n <= 1 else np.prod(np.arange(n, 0, -2)))

def factorial(n):
    return (1 if n <= 1 else np.prod(np.arange(n, 0, -1)))

def compute_spherical_harmonic_wavelet_normalizing_constant(l):
    if l % 2 == 0:
        return np.pi / (power(2,(l-1)/2) * np.sqrt(2*l+1) * factorial((l+1)/2))
    else:
        return np.sqrt(8 * np.pi) / (np.sqrt(2*l+1) * double_factorial(l+1))
