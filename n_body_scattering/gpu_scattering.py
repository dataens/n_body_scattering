import numpy as np
import pycuda.autoinit
from pycuda import gpuarray
from pycuda.elementwise import ElementwiseKernel
from pycuda.compiler import SourceModule
from pycuda.cumath import sqrt
import skcuda.fft as cu_fft
from itertools import product
from collections import defaultdict
from pycuda.tools import context_dependent_memoize, dtype_to_ctype
import numbers

NP_COMPLEX = np.complex64
COMPLEX = 'complex64'

class keydefaultdict(defaultdict):
    """Initialize class with keys"""
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        else:
            ret = self[key] = self.default_factory(key)
            return ret

# use this class to make a dictionary of fft plans
def get_fft_plan(args):
    return cu_fft.Plan(*args)
fft_plans = keydefaultdict(get_fft_plan)


def fourier_downsample(fsignal, axis, log_ds_factor=1):

    if isinstance(axis, numbers.Number):
        axis = axis,

    if isinstance(log_ds_factor, numbers.Number):
        log_ds_factor = log_ds_factor,

    if len(log_ds_factor) == 1:
        log_ds_factor = log_ds_factor * len(axis)

    ds_factor = 2 ** np.array(log_ds_factor).astype(int)
    ds_of_axis = {a: ds for a, ds in zip(axis, ds_factor)}

    pre_ds_shape = []
    summable_axes = []
    counter = 0
    for a, s in enumerate(fsignal.shape):
        if a in axis:
            pre_ds_shape.extend((ds_of_axis[a], s // ds_of_axis[a]))
            summable_axes.append(counter)
            counter = counter + 2
        else:
            pre_ds_shape.append(s)
            counter = counter + 1

    reshaped_f_signal = fsignal.reshape(pre_ds_shape)
    return reshaped_f_signal.mean(axis=tuple(summable_axes))


periodize = fourier_downsample

def check_signal_dim(signal_dim, fsignal_shape, fwavelets_shape):
    """Helper to verify or infer the dimensionality of the signal.

    Parameters
    ==========

    fsignal_shape: iterable

    fwavelet_shape: iterable

    Returns
    =======

    signal_dim: int

    Notes
    =====

    Assumptions are: signal axes are last 1, 2 or 3 axes. Since the
    implementation of a convolution in Fourier requires pointwise
    multiplication, the array shapes have to correspond on these axes.
    So the assumption of the dimensionality corresponds to the number of
    contiguous corresponding axes counting from the right."""

    rev_shapes_equal = np.array([int(d1 == d2) for d1, d2 in
                    zip(fsignal_shape[::-1], fwavelets_shape[::-1])])
    equal_up_to_ind = np.cumprod(rev_shapes_equal)
    if signal_dim is None:
        signal_dim = equal_up_to_ind.sum()
    else:
        if not (signal_dim <= equal_up_to_ind.sum()):
            raise ValueError("At signal dimension {} the shapes do not "
                                "correspond: {} != {}".format(signal_dim,
                                    fsignal_shape, fwavelets_shape))
    return signal_dim


@context_dependent_memoize
def get_general_memcopy_kernel(dtype):

    tp = dtype_to_ctype(dtype)
    _general_memcopy_module = SourceModule("""
#include <pycuda-complex.hpp>
// general copies of strided arrays
// can be used for subsampling. But current implementation will not work inplace

__global__ void _general_mem_copy({tp} *in_arr, {tp} *out_arr, int *shape,
                        int *strides_in, int *strides_out, int ndim){{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int total_threads = gridDim.x * blockDim.x;
    int shape_cumprod = total_threads; // initialized this way to initiate while
    int shape_cumprod_previous = 1;
    int data_index_in = 0;
    int data_index_out = 0;
    int axis_index;
    int itemsize = sizeof(*in_arr);
    while (shape_cumprod > idx){{
        shape_cumprod = 1;
        data_index_in = 0;
        data_index_out = 0;
        for (int j=ndim - 1; j >= 0; j--){{
            shape_cumprod_previous = shape_cumprod;
            shape_cumprod *= shape[j];
            axis_index = (idx % shape_cumprod) / shape_cumprod_previous;
            data_index_in += (strides_in[j] / itemsize) * axis_index;
            data_index_out += (strides_out[j] / itemsize) * axis_index;
        }}

        if (idx < shape_cumprod){{ // maybe if statement obselete due to while
            out_arr[data_index_out] = in_arr[data_index_in];
        }}
        idx += total_threads;
    }}
}}
""".format(tp=tp))
    _general_memcopy = _general_memcopy_module.get_function('_general_mem_copy')
    def general_memcopy(source_gpu_array, target_gpu_array, gpu_shape=None,
                        gpu_strides_source=None, gpu_strides_target=None,
                        block=None, grid=None):
        if source_gpu_array.dtype != dtype:
            raise ValueError("Expecting {dtype} as input, got {wrong}".format(
                dtype=dtype, wrong=source_gpu_array.dtype))

        if target_gpu_array.dtype != dtype:
            raise ValueError("Expecting {dtype} as input, got {wrong}".format(
                dtype=dtype, wrong=target_gpu_array.dtype))

        if gpu_shape is None:
            source_shape, target_shape = source_gpu_array.shape, target_gpu_array.shape
            if source_shape != target_shape:
                raise ValueError("Both source and target must be of same shape")
            gpu_shape = gpuarray.to_gpu(np.array(source_shape).astype('int32'))

        if gpu_strides_source is None:
            gpu_strides_source = gpuarray.to_gpu(
                    np.array(source_gpu_array.strides).astype('int32'))

        if gpu_strides_target is None:
            gpu_strides_target = gpuarray.to_gpu(
                    np.array(target_gpu_array.strides).astype('int32'))

        if block is None:
            block = source_gpu_array._block
        if grid is None:
            grid = source_gpu_array._grid

        _general_memcopy(np.int64(source_gpu_array.ptr),
                            np.int64(target_gpu_array.ptr),
                            np.int64(gpu_shape.ptr),
                            np.int64(gpu_strides_source.ptr),
                            np.int64(gpu_strides_target.ptr),
                            np.int64(len(gpu_shape)),
                            block=block, grid=grid
                            )
    return general_memcopy


memcopy_complex = get_general_memcopy_kernel(NP_COMPLEX)




from pycuda import driver
# another try: simple function for layer 2 moduli. make sure everything is
# allocated before or issue warning

#complex_mul = ElementwiseKernel(
#        ("pycuda::complex<float> *x, "
#         "pycuda::complex<float> *y, "
#         "pycuda::complex<float> *z"),
#         "z[i] = x[i] * y[i]",
#         "complex_mul",
#         preamble="#include <pycuda-complex.hpp>"
#        )
#def complex_mult(one_thing, several_things, output, signal_dim=None):
#
##    complex_mul = ElementwiseKernel(
##            ("pycuda::complex<float> *x, "
##             "pycuda::complex<float> *y, "
##             "pycuda::complex<float> *z"),
##             "z[i] = x[i] * y[i]",
##             "complex_mul",
##             preamble="#include <pycuda-complex.hpp>"
##            )
#    signal_dim = check_signal_dim(signal_dim, one_thing.shape,
#            several_things.shape)
#    one_thing_ = one_thing.reshape((-1,) + one_thing.shape[-signal_dim:])
#    several_things_ = several_things.reshape((-1,) +
#                            several_things.shape[-signal_dim:])
#    output_ = output.reshape((-1,) + output.shape[-signal_dim:])
#
#    assert one_thing_.shape[0] == 1
#    for i in range(several_things_.shape[0]):
#        complex_mul(one_thing_, several_things_[i], output_[i])
#

from skcuda import linalg as culinalg
culinalg.init()
def complex_mult(one_thing, several_things, output, signal_dim=None):
    # this uses high level skcuda which forces a copy :/
    # but maybe it is better than the handmade kernel
    signal_dim = check_signal_dim(signal_dim, one_thing.shape,
            several_things.shape)
    one_thing_ = one_thing.reshape((-1,) + one_thing.shape[-signal_dim:])
    several_things_ = several_things.reshape((-1,) +
            several_things.shape[-signal_dim:])
    output_ = output.reshape((-1,) + output.shape[-signal_dim:])
    assert one_thing_.shape[0] == 1
    one_thing__ = one_thing.ravel()
    several_things__ = several_things_.reshape(several_things_.shape[0], -1).T
    output__ = output_.reshape(several_things__.T.shape).T

    output_[:] = several_things_[:]
    culinalg.dot_diag(one_thing__, output__, overwrite=True)



complex_modulus_squared = ElementwiseKernel(
        "pycuda::complex<float> *x, pycuda::complex<float> *y",
        "y[i] = x[i] * conj(x[i])",
        "complex_modulus_squared",
        preamble="#include <pycuda-complex.hpp>")

#@profile
def gpu_layer(gpu_layer_moduli, fft_filter_group,
        log_ds_factor=None,
        conv_output_array=None,
        subsampling_array=None,
        accumulator_array=None,
        filter_gpu_array=None,
        signal_dim=None,
        fft_plans=None,
        moduli_are_in_fourier=False,
        verbose=0):

    signal_dim = check_signal_dim(signal_dim,
                                    gpu_layer_moduli.shape,
                                    fft_filter_group.shape)
    signal_shape = gpu_layer_moduli.shape[-signal_dim:]
    gpu_layer_moduli_ = gpu_layer_moduli.reshape(
            (-1,) + signal_shape)
    fft_filter_group_ = fft_filter_group.reshape(
            (-1,) + signal_shape)
    data_batch_size = gpu_layer_moduli_.shape[0]
    n_filters = fft_filter_group_.shape[0]

    fft_plan = fft_plans[signal_shape,
                NP_COMPLEX, NP_COMPLEX, data_batch_size]

    if verbose > 9:
        start = driver.Event()
        end = driver.Event()

    if conv_output_array is None:
        import warnings
        warnings.warn('conv_output_array was not specified. '
                'Allocating {} of dtype {}'.format(
                gpu_layer_moduli.shape, gpu_layer_moduli.dtype))
        conv_output_array = gpuarray.empty_like(gpu_layer_moduli)

    subsampling_shape = tuple(map(int,
        np.array(signal_shape) // 2 ** (log_ds_factor or 0)))
    if subsampling_array is None and log_ds_factor is not None:
        import warnings
        warnings.warn("Downsampling factor specified but no target "
                "array given. Allocating {}".format(
                    subsampling_shape))
        subsampling_array = gpuarray.empty(subsampling_shape,
                dtype=conv_output_array.dtype)
    elif subsampling_array is not None:
        if subsampling_array.shape != ((data_batch_size,) +
                                                subsampling_shape):
            raise ValueError("Subsampling array is of shape {} "
                    "but should be of shape {}".format(
                        subsampling_array.shape,
                        (data_batch_size,) + subsampling_shape))
    else: # this else condition is both are None
        subsampling_array = conv_output_array


    if accumulator_array is None and n_filters > 1:
        import warnings
        warnings.warn("Need accumulator array to gather squared "
                "moduli. Allocating {}".format(
                    subsampling_array.shape))
        accumulator_array = gpuarray.empty_like(subsampling_array)

    if accumulator_array is not None:
        accumulator_array.fill(0.)

    if filter_gpu_array is None:
        import warnings
        warnings.warn('Allocating contiguous gpu space for filter'
                ' of shape {}'.format(signal_shape))
        filter_gpu_array = gpuarray.empty(signal_shape,
                dtype=gpu_layer_moduli.dtype)

    # fft if necessary
    if not moduli_are_in_fourier:
        cu_fft.fft(gpu_layer_moduli, gpu_layer_moduli, fft_plan)

    # iterate over filters in group and accumulate
    for fi in range(n_filters):
        filter_gpu_array[:] = fft_filter_group[fi]
        # filter multiplication against all data points
        if verbose > 9:
            start.record()
        complex_mult(filter_gpu_array, gpu_layer_moduli,
                conv_output_array)
        if verbose > 9:
            end.record()
            end.synchronize()
            print("Elementwise multiplication took {}".format(start.time_till(end)))
        # inverse fourier transform
        if verbose > 9:
            start.record()
        cu_fft.ifft(conv_output_array, conv_output_array, fft_plan, scale=True)
        if verbose > 9:
            end.record()
            end.synchronize()
            print("Ifft took {}".format(start.time_till(end)))

        # subsampling
        if log_ds_factor is not None:
            ds_factor = 2 ** log_ds_factor
            ds_slices = ((slice(None),) + (slice(0, None, ds_factor),) * signal_dim)
            memcopy_complex(conv_output_array[ds_slices], subsampling_array)

        # complex modulus squared
        complex_modulus_squared(subsampling_array, subsampling_array)

        # accumulation
        if accumulator_array is not None:
            accumulator_array += subsampling_array
    sqrt(accumulator_array, out=accumulator_array)


from pycuda import elementwise
def pow(in_arr, p, out_arr=None, stream=None):
    if out_arr is None:
        out_arr = in_arr
    func = elementwise.get_pow_kernel(in_arr.dtype)
    func.prepared_async_call(in_arr._grid, in_arr._block, stream,
                                p, in_arr.gpudata, out_arr.gpudata, in_arr.mem_size)


from skcuda.misc import sum as cusum
from pycuda.tools import make_default_context
#@profile
def scattering(data, filters1, filters2=None, Js1=None, Js2=None,
        Rs1=None, Rs2=None, filter_groups1=None, filter_groups2=None,
        scattering_paths=None, integral_powers=(1., 2.),
        data_batch_size=15, signal_dim=None, verbose=0, debug=False,
        auto_init=False,fspace_data=True, all_scales_on_layer_2=False):
    """Performs the scattering transform on data using the given filters.
    
    Args:
        data (numpy.array): An nd-array with the signal being in the last 1,2, or 3 axes.    
        filters1 (numpy.array): The filters used for the first order scattering transform.
        filters2 (numpy.array, optional): The filters used for the second order scattering transform
        Js1 (numpy.array, optional): Same shape as filters1[:-signal_dim]. Provides the scale j of each filter in the first order filter bank
        Js2 (numpy.array, optional): Same shape as filters2[:-signal_dim]. Provides the scale j of each filter in the second order filter bank
        Rs1 (numpy.array, optional): Same shape as filters1[:-signal_dim]. Provides the subsampling of performed before filtering for each filter for the first order.
        Rs2 (numpy.array, optional): Same shape as filters2[:-signal_dim]. Provides the subsampling of performed before filtering for each filter for the second order.
        filter_groups1 (numpy.array, optional): Same shape as filters1[:-signal_dim]. Associates wavelets with a group. Filters belonging to the same group are added up to generate invariance(useful in a 3D context) 
        filter_groups2 (numpy.array, optional): Same shape as filters2[:-signal_dim]. Associates wavelets with a group. Filters belonging to the same group are added up to generate invariance(useful in a 3D context)
        scattering_paths (numpy.array, optional): Explicit definition of scattering paths over the groups.
        integral_powers (tuple, optional): The power of the integrals used to get the invariant representation.
        data_batch_size (int, optional): Batch size to load in a GPU.
        signal_dim (Int, optional): Dimensionality of the signal.
        verbose (int, optional): Verbosity level.
        debug (bool, optional): Creates additional output for debugging.
        auto_init (bool, optional): generates a new pycuda context.
        fspace_data (bool, optional): True means  the data are already in fourier space. Defaults to True. 
        all_scales_on_layer_2 (bool, optional): True will use all scales for order 2 coefficients (not recommended). Defaults to False.
    
    Returns:
        numpy.array: numpy array of the same shape as data[:-sigmal_dim]+(#coefficients,#integral_powers) with the scattering coefficients.
    
    Raises:
        ValueError: Errors are raised if some sanity checks fail.
    """
    if auto_init:
        ctx = make_default_context()

    signal_dim = check_signal_dim(signal_dim, data.shape, filters1.shape)
    signal_shape = data.shape[-signal_dim:]
    if verbose > 0:
        print("data shape {}, filter shape {}, signal shape {}".format(
            data.shape, filters1.shape, signal_shape))
    if Js1 is None:
        # set the Js according to the first axis of the filters
        Js1 = np.zeros(filters1.shape[:-signal_dim], dtype=int)
        Js1.T[:] = np.arange(Js1.shape[0])

    if filter_groups1 is None:
        # Just assume every filter is independent
        # should we say 3D --> assume spherical harmonics?
        filter_groups1 = np.arange(
                np.prod(filters1.shape[:-signal_dim])
                    ).reshape(filters1.shape[:-signal_dim])

    if Rs1 is None:
        Rs1 = np.maximum(Js1 - 1 , 0)

    if filters2 is None:
        filters2 = filters1
        if Js2 is None:
            Js2 = Js1
        if filter_groups2 is None:
            filter_groups2 = filter_groups1
        if Rs2 is None:
            Rs2 = Rs1

    if not isinstance(filters2, dict):
        # check if we have a dictionary of resolutions. If not, make one
        necessary_res = np.unique(Rs1)
        filters2_ = {0: filters2}
        for tr in necessary_res:
            if tr == 0:
                continue
            else:
## this is spatial downsampling. But filters are in fourier!!!!!
#                slices = ([slice(None)] * (filters2.ndim - signal_dim)
#                        + [slice(0, None, 2 ** tr)] * signal_dim)
#                filters2_[tr] = filters2[slices]

                filters2_[tr] = fourier_downsample(filters2,
                            axis=tuple(range(filters2.ndim - signal_dim,
                                            filters2.ndim)),
                            log_ds_factor=tr) * 2 ** (signal_dim * tr)
        filters2 = filters2_

    if Js2 is None:
        Js2 = np.zeros(filters2[0].shape[:-signal_dim], dtype=int)
        Js2.T[:] = np.arange(Js2.shape[0])

    if Rs2 is None:
        Rs2 = np.maximum(Js2 - 1, 0)

    if filter_groups2 is None:
        # assume filters are independent
        filter_groups2 = np.arange(np.prod(filters2[0].shape[:-signal_dim])
                ).reshape(filters2[0].shape[:-signal_dim])


    if scattering_paths is None:
        layer1_paths = list(map(tuple,
                                np.unique(filter_groups1)[:, np.newaxis]))
        j_of_filter1 = defaultdict(lambda *x: None)
        for fg, j in zip(np.nditer(filter_groups1), np.nditer(Js1)):
            if isinstance(fg, np.ndarray):
                fg = fg.item()
            j_of_filter1[fg] = (j_of_filter1[fg] and None) or j
            if j_of_filter1[fg] is None:
                raise ValueError(
                        "Some filter1 groups have non-identical scale")

        j_of_filter2 = defaultdict(lambda *x: None)
        for fg, j in zip(np.nditer(filter_groups2), np.nditer(Js2)):
            if isinstance(fg, np.ndarray):
                fg = fg.item()
            j_of_filter2[fg] = (j_of_filter2[fg] and None) or j
            if j_of_filter2[fg] is None:
                raise ValueError(
                        "Some filter2 groups have non-identical scale")

        layer2_paths = []
        for f1, f2 in product(np.unique(filter_groups1),
                                    np.unique(filter_groups2)):
            f1 = np.array(f1).item()
            f2 = np.array(f2).item()  # these are to catch weird dtypes
            if all_scales_on_layer_2 or j_of_filter2[f2] > j_of_filter1[f1]:
                layer2_paths.append((f1, f2))

        scattering_paths = layer1_paths + layer2_paths
    if len(scattering_paths) == 0: # make sure we have work to do
        return

    ################################
    ## allocate all necessary memory
    ################################
    gpu_data_array = gpuarray.empty((data_batch_size,) + signal_shape,
                        dtype=COMPLEX)
    if verbose > 0:
        size_data = gpu_data_array.size * gpu_data_array.dtype.itemsize
        print("Allocated data array {} - {} bytes, {}M".format(
            gpu_data_array.shape, size_data, size_data / 1024 ** 2))

    gpu_l1_array = gpuarray.empty_like(gpu_data_array)
    if verbose > 0:
        size_l1 = gpu_l1_array.size * gpu_l1_array.dtype.itemsize
        print("Allocated layer 1 array {} - {} bytes, {}M".format(
            gpu_l1_array.shape, size_l1, size_l1 / 1024 ** 2))

    gpu_subsamp_array = gpuarray.empty_like(gpu_data_array)
    # this array is to make subsampled array contiguous again. It is
    # thus not necessary if there is no subsampling. This is the case
    # for full resolution work and on the first layers of multiresolution
    # work. So actually one would only need to allocate 1/8x that memory.
    if verbose > 0:
        size_subs = gpu_subsamp_array.size * gpu_subsamp_array.dtype.itemsize
        print("Allocated subsampling array {} - {} bytes, {}M".format(
            gpu_subsamp_array.shape, size_subs, size_subs / 1024 ** 2))

    gpu_acc_array = gpuarray.empty_like(gpu_data_array)
    # accumulator array is not necessary if every filter group only has
    # one element
    if verbose > 0:
        size_acc = gpu_acc_array.size * gpu_acc_array.dtype.itemsize
        print("Allocated accumulator array {} - {} bytes, {}M".format(
            gpu_acc_array.shape, size_acc, size_acc / 1024 ** 2))

    gpu_l2_array = gpuarray.empty_like(gpu_data_array)
    if verbose > 0:
        size_l2 = gpu_l2_array.size * gpu_l2_array.dtype.itemsize
        print("Allocated layer 2 array {} - {} bytes, {}M".format(
            gpu_l2_array.shape, size_l2, size_l2 / 1024 ** 2))

    gpu_filter_array = gpuarray.empty(signal_shape, dtype=COMPLEX)
    if verbose > 0:
        size_filter = gpu_filter_array.size
        print("Allocated filter array {} - {} bytes, {}M".format(
            gpu_filter_array.shape, size_filter, size_filter / 1024 ** 2))

    # also allocate all necessary fft plans. One per target resolution of
    # layer1 (although we should only allocate those used in layer2)
    fft_plans = {(signal_shape, NP_COMPLEX, NP_COMPLEX, data_batch_size):
            cu_fft.Plan(signal_shape, NP_COMPLEX, NP_COMPLEX, data_batch_size)}
    for tr in np.unique(Rs1):
        if tr != 0:
            subsamp_shape = tuple(map(int, np.array(signal_shape) // 2 ** tr))
            fft_plans[subsamp_shape, NP_COMPLEX, NP_COMPLEX, data_batch_size
                    ] = cu_fft.Plan(subsamp_shape,
                            NP_COMPLEX, NP_COMPLEX, data_batch_size)

    ################################
    ## end of memory allocations
    ################################
    gpu_l2_array_ = None
    gpu_acc_array_real = None

    # squeeze all leading axes into one
    # (using reshape instead of shape= because gpuarray is stupid and
    # it could be passed)
    data_ = data.reshape((-1,) + signal_shape)
    filters1_ = filters1.reshape((-1,) + signal_shape)
    filters2_ = {r: f.reshape((-1,) + f.shape[-signal_dim:])
            for r, f in filters2.items()}
    Js1_ = Js1.ravel()
    Js2_ = Js2.ravel()
    Rs1_ = Rs1.ravel()
    Rs2_ = Rs2.ravel()
    filter_groups1_ = filter_groups1.ravel()
    filter_groups2_ = filter_groups2.ravel()

    # determine what layer1 filter groups to loop over
    unique_l1_filter_groups, path_ind_ = np.unique(
            [p[0] for p in scattering_paths], return_inverse=True)

    output_ = np.zeros((data_.shape[0], len(scattering_paths), len(integral_powers)))

    # loop over data batches
    for i in range(0, data_.shape[0], data_batch_size):
        # copy data to gpu data array
        data_batch_size_ = min(data_batch_size, data_.shape[0] - i)
        if data_.dtype == gpu_data_array.dtype:
            gpu_data_array[:data_batch_size_] = data_[i:i + data_batch_size_]
        else:
            gpu_data_array[:data_batch_size_] = data_[i:i + data_batch_size_
                    ].astype(gpu_data_array.dtype)

        output_batch = output_[i:i + data_batch_size]

        # Fourier transform the data once from the data array in place
        # This way we make sure it never happens again.
        if not fspace_data:
            full_fft_plan = fft_plans[signal_shape, NP_COMPLEX, NP_COMPLEX,
                    data_batch_size]
            cu_fft.fft(gpu_data_array, gpu_data_array, full_fft_plan)

        # now we take each unique layer 1 filter group and write the
        # modulus into the layer1 data array, one by one.
        for ul1fg_ind, ul1fg in enumerate(unique_l1_filter_groups):
            if verbose > 1:
                print("Currently treating layer 1 index {}".format(ul1fg))
            # path_ind = np.where(path_ind_ == ul1fg_ind)
            filter1_ids = np.where(filter_groups1_ == ul1fg)[0]
            fil1 = filters1_[filter1_ids]

            j1 = Js1_[filter1_ids]
            assert len(np.unique(j1)) == 1
            j1 = j1[0]

            r1 = Rs1_[filter1_ids]
            assert len(np.unique(r1)) == 1
            r1 = r1[0]

            # crop some of the arrays to be written to to the appropriate
            # shapes, given by the current target resolution, r1
            subsamp_shape = (data_batch_size,) + tuple(
                        map(int, np.array(signal_shape) // 2 ** r1))
            gpu_subsamp_array_ = gpu_subsamp_array.ravel()[
                    :np.prod(subsamp_shape)].reshape(subsamp_shape)
            gpu_acc_array_ = gpu_acc_array.ravel()[
                    :np.prod(subsamp_shape)].reshape(subsamp_shape)

            # get the accumulated layer 1 modulus
            if verbose > 9:
                start = driver.Event()
                end = driver.Event()
                print("Starting convolution from fourier transformed data")
                start.record()
            gpu_layer(gpu_data_array, fil1,
                    log_ds_factor=r1,
                    conv_output_array=gpu_l1_array,
                    subsampling_array=gpu_subsamp_array_,
                    accumulator_array=gpu_acc_array_,
                    filter_gpu_array=gpu_filter_array,
                    signal_dim=signal_dim,
                    fft_plans=fft_plans,
                    moduli_are_in_fourier=True,
                    verbose=verbose)
            if verbose > 9:
                end.record()
                end.synchronize()

                print('Done convolution and subsampling in {}'.format(start.time_till(end)))
            # the accumulated layer 1 modulus is now in accumulator_array.
            # we place it back in gpu_l1_array, which needs to be duly
            # reshaped.
            gpu_l1_array_ = gpu_l1_array.ravel()[
                    :np.prod(subsamp_shape)].reshape(subsamp_shape)
            gpu_l1_array_[:] = gpu_acc_array_[:]

            # now we can safely evaluate in place power integrals on
            # layer 1 if required.
            if verbose > 9:
                print("Calculating integrals")
                start.record()
            l1_coef_pos = np.array([p == (ul1fg,) for p in scattering_paths])
            if l1_coef_pos.any():
                gpu_acc_array_.imag.fill(0.)
                old_power = 1.
                l1_coef = []
                for ip in integral_powers:
                    power_ratio = ip / old_power
                    old_power = ip
                    gpu_acc_array_reshaped = gpu_acc_array_.reshape(gpu_acc_array_.shape[0], -1)
                    gpu_acc_array_reshaped_real = gpu_acc_array_reshaped.view(dtype='float32')
                    pow(gpu_acc_array_reshaped_real, power_ratio)
                    #power_integrals = cusum(gpu_acc_array_reshaped_real[:, :, ::2], axis=1).get()
                    power_integrals = cusum(gpu_acc_array_reshaped_real, axis=1).get()
                    l1_coef.append(power_integrals)
                l1_coef = np.array(l1_coef) * 2 ** (signal_dim * r1)
                output_batch[:, l1_coef_pos, :] = l1_coef.T[:data_batch_size_, np.newaxis]
            if verbose > 9:
                end.record()
                end.synchronize()
                print("Done calculating integrals in {}".format(start.time_till(end)))

            # from here we can move on to the second layer
            # find all paths to layer 2 leading through this layer 1 modulus
            unique_l2_filter_groups, l2_ind_ = np.unique(
                    [p[1] for p in scattering_paths
                        if p[0] == ul1fg and len(p) == 2],
                    return_inverse=True)

            if len(unique_l2_filter_groups) == 0: # avoid recalculating fourier if unnecessary
                continue

            # create l2 array to accommodate fourier filter product
            gpu_l2_array_ = gpu_l2_array.ravel()[
                    :np.prod(gpu_l1_array_.shape)].reshape(gpu_l1_array_.shape)

            gpu_filter_array_ = gpu_filter_array.ravel()[
                    :np.prod(gpu_l1_array_.shape[1:])].reshape(
                            gpu_l1_array_.shape[1:])

            # do Fourier transform of layer 1 modulus, placed in gpu_l1_array_
            plan = fft_plans[gpu_l1_array_.shape[1:],
                    NP_COMPLEX, np.complex64, gpu_l1_array_.shape[0]]
            cu_fft.fft(gpu_l1_array_, gpu_l1_array_, plan)

            for ul2fg_ind, ul2fg in enumerate(unique_l2_filter_groups):
                l2_ind = np.where(l2_ind_ == ul2fg_ind)[0]
                filter2_ids = np.where(filter_groups2_ == ul2fg)[0]
                fil2 = filters2_[r1][filter2_ids]

                j2 = Js2_[filter2_ids]
                assert len(np.unique(j2)) == 1
                j2 = j2[0]

                r2 = Rs2_[filter2_ids]
                assert len(np.unique(r2)) == 1
                r2 = r2[0]

                subsamp_shape = (data_batch_size,) + tuple(
                            map(int, np.array(signal_shape) // 2 ** r2))
                gpu_subsamp_array_ = gpu_subsamp_array.ravel()[
                        :np.prod(subsamp_shape)].reshape(subsamp_shape)
                gpu_acc_array_ = gpu_acc_array.ravel()[
                        :np.prod(subsamp_shape)].reshape(subsamp_shape)


                if verbose > 9:
                    print("Starting convolution from fourier transformed data")
                    start.record()
                gpu_layer(gpu_l1_array_, fil2,
                        log_ds_factor=r2 - r1,
                        conv_output_array=gpu_l2_array_,
                        subsampling_array=gpu_subsamp_array_,
                        accumulator_array=gpu_acc_array_,
                        filter_gpu_array=gpu_filter_array_,
                        signal_dim=signal_dim,
                        fft_plans=fft_plans,
                        moduli_are_in_fourier=True,
                        verbose=verbose)
                if verbose > 9:
                    end.record()
                    end.synchronize()


                if verbose > 9:
                    print("Calculating integrals")
                    start.record()
                l2_coef_pos = np.array([p == (ul1fg, ul2fg)
                    for p in scattering_paths])
                gpu_acc_array_.imag.fill(0.)
                gpu_acc_array_reshaped = gpu_acc_array_.reshape(gpu_acc_array_.shape[0], -1)
                gpu_acc_array_real = gpu_acc_array_reshaped.view(dtype='float32')

                old_power = 1.
                l2_coef = []
                for ip in integral_powers:
                    power_ratio = ip / old_power
                    old_power = ip
                    pow(gpu_acc_array_real, power_ratio)
                    power_integrals = cusum(gpu_acc_array_real, axis=1).get()
                    l2_coef.append(power_integrals)
                l2_coef = np.array(l2_coef) * 2 ** (signal_dim * r2)
                output_batch[:, l2_coef_pos, :] = l2_coef.T[:data_batch_size_, np.newaxis]
                if verbose > 9:
                    end.record()
                    end.synchronize()
                    print("Done calculating integrals in {}".format(start.time_till(end)))

    if debug:
        return (output_, data, filters1, gpu_data_array, gpu_filter_array,
                gpu_l1_array, gpu_l2_array,
                gpu_subsamp_array, gpu_acc_array,
                gpu_l1_array_, gpu_l2_array_, gpu_subsamp_array_,
                gpu_acc_array_, gpu_acc_array_real ,layer1_paths,layer2_paths)

    return output_
