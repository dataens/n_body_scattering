import numpy as np
from pycuda import gpuarray, cumath, autoinit
from .scattering_utilities import factorial, double_factorial, get_real_and_complex_data_types
from .gpu_wavelets import compute_solid_harmonic_wavelet_on_gpu_grid
from .wavelets import get_3D_angles
from numpy import power


#@profile
def generate_gpu_fourier_densities(positions, properties, grid, gaussian_width, ifftshift=True,check_args=True, tol=1e-6):
    """
    Parameters:
        positions: 1D array of n_sytems objects
            objects are 2D array of size n_bodies x dimension

        properties: 1D array of n_sytems objects
            objects are 2D array of size n_bodies x n_property

        gpu_grid: ndgrid on gpu

        gaussian_width: float
            width of the gaussian representing the individual bodies
    ==========
    Returns:
        gaussian densities: gpu array of size n_systems x n_properties x n1 x n2 (x n3)
    """
    if check_args:
        assert positions.shape[0] == properties.shape[0], "Number of positions {} different from number of properties {}".format(positions.shape[0], properties.shape[0])
        assert grid.shape[0] == 3 or grid.shape[0] == 2, "space dimension must be two or 3"
        assert (np.max(grid) < np.pi + tol) and (np.min(grid) > -np.pi - tol), "grid is not contained in [-pi, +pi]"

    gpu_grid = gpuarray.to_gpu(grid.astype('float32'))

    n_systems = positions.shape[0]
    property_size = properties[0].shape[1]
    signal_dim = positions[0].shape[1]

    signal_shape = gpu_grid.shape[1:]
    fourier_density_shape = (n_systems, property_size,) + signal_shape

    # allocating ressources on GPU
    gpu_fourier_densities = gpuarray.zeros(fourier_density_shape, dtype='complex64')
    gpu_fourier_gaussian = gpuarray.zeros(signal_shape, dtype='float32')
    gpu_dot_product = gpuarray.zeros(signal_shape, dtype='float32')
    gpu_exp_dot_product_real_part = gpuarray.zeros(signal_shape, dtype='float32')
    gpu_exp_dot_product_imag_part = gpuarray.zeros(signal_shape, dtype='float32')

    #compute the fourier Gaussian
    for i_coord in range(signal_dim):
        gpu_fourier_gaussian += gpu_grid[i_coord] * gpu_grid[i_coord]
    gpu_fourier_gaussian *= -gaussian_width**2
    cumath.exp(gpu_fourier_gaussian, out=gpu_fourier_gaussian)

    for i_system in range(n_systems):
        body_positions = positions[i_system]
        body_properties = properties[i_system].astype('complex64')
        body_properties_times_minus_j = (-1.j * properties[i_system]).astype('complex64')
        n_pos = body_positions.shape[0]
        n_props = body_properties.shape[0]
        sys_prop_size = body_properties.shape[1]

        if check_args:
            assert n_pos == n_props, "system {} number of positions ({}) and properties ({}) don't match".format(i_system, n_pos, n_props)
            assert sys_prop_size == property_size, "system {} property size ({}) doesn't match first system property_size ({})".format(i_system, sys_prop_size, property_size)

        n_bodies = n_pos

        for i_body in range(n_bodies):
            body_position = body_positions[i_body]
            body_property = body_properties[i_body]
            if np.abs(body_property).sum()==0:
                continue
            body_property_times_minus_j = body_properties_times_minus_j[i_body]
            gpu_dot_product.fill(0.)
            gpu_exp_dot_product_real_part.fill(0.)
            gpu_exp_dot_product_imag_part.fill(0.)

            #compute the complex exponential of dot product
            for i_coord in range(signal_dim):
                gpu_dot_product += body_position[i_coord] * gpu_grid[i_coord]
            cumath.cos(gpu_dot_product, out=gpu_exp_dot_product_real_part)
            cumath.sin(gpu_dot_product, out=gpu_exp_dot_product_imag_part)

            for i_property in range(property_size):
                gpu_fourier_densities[i_system, i_property] += body_property[i_property] * gpu_exp_dot_product_real_part.astype('complex64')
                gpu_fourier_densities[i_system, i_property] += body_property_times_minus_j[i_property] * gpu_exp_dot_product_imag_part.astype('complex64')

        for i_property in range(property_size):
            gpu_fourier_densities[i_system, i_property] *= gpu_fourier_gaussian

    if ifftshift:
        import scipy.fftpack as fft
        cpu_fourier_densities=gpu_fourier_densities.get()
        gpu_fourier_densities.set(fft.ifftshift(cpu_fourier_densities,axes=np.arange(2,len(gpu_fourier_densities.shape))))

    return gpu_fourier_densities

def generate_gpu_first_order_space_density(centers, properties, grid, gaussian_width, wavelet_sigma, wavelet_l, wavelet_j):
    assert centers.shape[0] == properties.shape[0],  "number of centers {} and of properties {} must be the same".format(centers.shape[0], properties.shape[0])

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision=True)

    n_centers = centers.shape[0]
    n_properties = properties.shape[1]
    density_shape = (n_properties,) + grid.shape[1:]

    sigma_Psi = np.sqrt((wavelet_sigma * 2**wavelet_j)**2 + gaussian_width**2)
    constant = (wavelet_sigma * 2**wavelet_j / sigma_Psi)**wavelet_l

    gpu_density = gpuarray.zeros(density_shape, real_dtype)

    for wavelet_m in range(-wavelet_l, wavelet_l+1):
        gpu_m_density = gpuarray.zeros(density_shape, dtype=complex_dtype)
        gpu_single_density = gpuarray.zeros(grid.shape[1:], dtype=complex_dtype)
        for i_center in range(n_centers):
            center = centers[i_center]
            centered_grid = (grid - center.reshape((3, 1, 1, 1))).astype(real_dtype)
            r_grid = np.sqrt((centered_grid ** 2).sum(0))
            polar_grid, azimuthal_grid = get_3D_angles(centered_grid)

            gpu_r_grid = gpuarray.to_gpu(r_grid)
            gpu_polar_grid = gpuarray.to_gpu(polar_grid)
            gpu_azimuthal_grid = gpuarray.to_gpu(azimuthal_grid)

            compute_solid_harmonic_wavelet_on_gpu_grid(gpu_r_grid, gpu_polar_grid, gpu_azimuthal_grid, sigma_Psi, wavelet_l, wavelet_m, gpu_single_density)
            gpu_single_density *= constant
            prop = properties[i_center].astype(complex_dtype)

            for i_prop in range(n_properties):
                gpu_m_density[i_prop] += prop[i_prop] * gpu_single_density

        gpu_density += gpu_m_density.real**2 + gpu_m_density.imag**2

    return cumath.sqrt(gpu_density)
