"""Functions  to generate densities on CPU."""
import numpy as np
from numpy import power
from .wavelets import _f_gaussian, get_2D_angles, get_3D_angles
from .scattering_utilities import factorial, double_factorial, compute_spherical_harmonic_wavelet_normalizing_constant, get_real_and_complex_data_types
from scipy.special import sph_harm


def generate_space_densities(positions, properties, grid, gaussian_width, single_precision=True):
    """Generate the densities in Fourier space on the grid.

    Args:\
        positions: array of size (n_molecules,), spatial positions of the atoms of the molecules
        properties: array of size (n_molecules,), properties of the atoms of the molecules
                    (charge, n_valence_electrons...)
        grid: array, numerical grid in space
        gaussian_width: float, width of the gaussian functions used in space
        single_precision: Boolean, if False, double precision is used

    Returns:
        densities: array containing the densities generated in space
    """
    assert positions.shape[0] == properties.shape[0], \
           "Number of positions {} different from number of properties {}".format(
               positions.shape[0], properties.shape[0])

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    n_dims = grid.shape[0]
    n_systems = positions.shape[0]
    property_size = properties[0].shape[1]
    densities_shape = (n_systems, property_size) + grid.shape[1:]
    densities = np.zeros(densities_shape, dtype=real_dtype)


    for i_system in range(n_systems):
        n_bodies = positions[i_system].shape[0]
        for i_body in range(n_bodies):
            body_position = positions[i_system][i_body].astype(real_dtype)
            body_property = properties[i_system][i_body].astype(real_dtype)
            position_centered_grid = grid.copy().astype(real_dtype)

            for i_dim in range(n_dims):
                position_centered_grid[i_dim] -= body_position[i_dim]

            gaussian = np.exp(-0.5 * (position_centered_grid**2).sum(0) / gaussian_width**2)
            densities[i_system] += np.tensordot(body_property, gaussian, axes=0)

    return densities


def generate_fourier_densities(positions, properties, grid, gaussian_width, single_precision=True):
    """Generate the densities in Fourier space on the grid.

    Args:\
        positions: array of size (n_molecules,), spatial positions of the atoms of the molecules
        properties: array of size (n_molecules,), properties of the atoms of the molecules
                    (charge, n_valence_electrons...)
        grid: array, numerical grid in Fourier space
        gaussian_width: float, width of the gaussian functions used in space
        single_precision: Boolean, if False, double precision is used

    Returns:
        fourier_densities: array containing the densities generated in Fourier space
    """
    assert positions.shape[0] == properties.shape[0], \
           "Number of positions {} different from number of properties {}".format(
               positions.shape[0], properties.shape[0])
    assert (grid.max() <= 3.15) and (grid.min() >= -3.15), "grid is not contained in [-pi, +pi]"

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    n_systems = positions.shape[0]
    property_size = properties[0].shape[1]
    fourier_densities_shape = (n_systems, property_size) + grid.shape[1:]
    fourier_densities = np.zeros(fourier_densities_shape, dtype=complex_dtype)
    grid = grid.astype(real_dtype)

    fourier_gaussian = np.exp(-0.5 * gaussian_width * (grid**2).sum(0)).astype(complex_dtype)

    for i_system in range(n_systems):
        body_positions = positions[i_system]
        body_properties = properties[i_system]
        n_pos = body_positions.shape[0]
        n_props = body_properties.shape[0]
        sys_prop_size = body_properties.shape[1]

        assert n_pos == n_props, \
                "system {} number of positions ({}) and properties ({}) don't match".format(
                    i_system, n_pos, n_props)
        assert sys_prop_size == property_size, \
               "system {} property size ({}) doesn't match first system property_size ({})".format(
                   i_system, sys_prop_size, property_size)

        n_bodies = n_pos

        for i_body in range(n_bodies):
            body_position = body_positions[i_body].reshape((3, 1, 1, 1)).astype(real_dtype)
            i_r_dot_omega = (-1j * (body_position * grid).sum(0)).astype(complex_dtype)
            exp_i_r_dot_omega = np.exp(i_r_dot_omega)

            body_property = body_properties[i_body].astype(complex_dtype)
            fourier_densities[i_system] += np.tensordot(body_property, exp_i_r_dot_omega, axes=0)

        fourier_densities[i_system] *= fourier_gaussian

    return np.fft.ifftshift(fourier_densities)


def generate_first_order_single_gaussian_density(center, gaussian_width, grid, wavelet_sigma, wavelet_l, wavelet_m, wavelet_j, single_precision=True):
    """Generate the first order density of a single atom.

    Args:
        center: array, center of the atom
        gaussian_width: float, width of the gaussian function
        grid: numerical grid in space
        wavelet_sigma: float, sigma parameter of the wavelet
        wavelet_l: Integer, first index of the spherical harmonic
        wavelet_m: Integer, second index of the spherical harmonic
        wavelet_j: Integer, scale of the wavelet
        single_precision: Boolean, if not, double precision used

    Returns:
        density
    """

    assert grid.shape[0] == 3, "grid must be 3-dimensionnal, got {} dim grid"

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    K_l = compute_spherical_harmonic_wavelet_normalizing_constant(wavelet_l)
    sigma_Psi = np.sqrt((wavelet_sigma * 2**wavelet_j)**2 + gaussian_width**2)
    sigma_wavelet_j = wavelet_sigma * 2**wavelet_j
    constant = K_l / (power(2 * np.pi, 1.5) * sigma_Psi**3) * (sigma_wavelet_j / sigma_Psi)**wavelet_l

    centered_grid = (grid - center.reshape((3, 1, 1, 1))).astype(real_dtype)
    r_grid = np.sqrt((centered_grid ** 2).sum(0))
    polar_grid, azimuthal_grid = get_3D_angles(centered_grid)

    radial_part = np.exp(-r_grid**2 / (2 * sigma_Psi**2)) * (r_grid / sigma_Psi)**wavelet_l
    angular_part = sph_harm(wavelet_m, wavelet_l, azimuthal_grid, polar_grid)

    return  constant * radial_part * angular_part


def generate_first_order_space_density(centers, properties, grid, gaussian_width, wavelet_sigma, wavelet_l, wavelet_j, single_precision=True, output_array=None):
    """Generate the first order density of a single atom.

    Args:
        centers: array, centers of the atom
        properties: array, properties of the atoms (charges, n_valence electrons...)
        grid: numerical grid in space
        gaussian_width: float, width of the gaussian function
        wavelet_sigma: float, sigma parameter of the wavelet
        wavelet_l: Integer, first index of the spherical harmonic
        wavelet_m: Integer, second index of the spherical harmonic
        wavelet_j: Integer, scale of the wavelet
        single_precision: Boolean, if not, double precision used
        output_array: array to store the output

    Returns:
        density, if no output_array provided
    """
    assert centers.shape[0] == properties.shape[0],  "number of centers {} and of properties {} must be the same".format(centers.shape[0], properties.shape[0])

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    n_centers = centers.shape[0]
    n_properties = properties.shape[1]
    density_shape = (n_properties,) + grid.shape[1:]

    if output_array is None:
        density = np.zeros(density_shape, dtype=real_dtype)
    else:
        assert output_array.shape == density_shape, "output array must have {} shape, got {}".format(density_shape, output_array.shape)
        density = output_array

    for wavelet_m in range(-wavelet_l, wavelet_l+1):
        m_density = np.zeros(density_shape, dtype=complex_dtype)
        for i_center in range(n_centers):
            center = centers[i_center].astype(real_dtype)
            single_density = generate_first_order_single_gaussian_density(center, gaussian_width, grid, wavelet_sigma, wavelet_l, wavelet_m, wavelet_j, single_precision=single_precision)
            prop = properties[i_center].astype(complex_dtype)
            m_density += np.tensordot(prop, single_density, axes=0)

        density += np.abs(m_density)**2

    if output_array is None:
        return np.sqrt(density)
    else:
        np.sqrt(density, out=density)


def generate_first_order_space_densities(positions, properties, grid, gaussian_width,
                                         wavelet_sigma, wavelet_l, wavelet_j,
                                         single_precision=True):
    """Generate the first order densities of molecules."""
    assert positions.shape[0] == properties.shape[0],  "number of positions {} and of properties {} must be the same".format(positions.shape[0], properties.shape[0])

    real_dtype, complex_dtype = get_real_and_complex_data_types(single_precision)

    n_positions = positions.shape[0]
    property_size = properties[0].shape[1]
    densities_shape = (n_positions, property_size,) + grid.shape[1:]
    densities = np.zeros(densities_shape, dtype=real_dtype)

    for i_position in range(n_positions):
        centers = positions[i_position]
        props = properties[i_position]
        generate_first_order_space_density(centers, props, grid, gaussian_width, wavelet_sigma, wavelet_l, wavelet_j, single_precision=single_precision, output_array=densities[i_position])

    return densities


def compute_second_order_density(fourier_first_order_density, fourier_wavelets, return_square=False):
    """Computes the second order density given the first order one in Fourier and the wavelets."""
    assert fourier_first_order_density.shape[1:] == fourier_wavelets.shape[1:], "density and wavelets must have same spatial shape"
    n_wavelets = fourier_wavelets.shape[0]
    n_channels = fourier_first_order_density.shape[0]
    second_order_density = np.zeros_like(fourier_first_order_density)

    for i_channel in range(n_channels):
        for i_wavelet in range(n_wavelets):
            second_order_density[i_channel] += np.abs(np.fft.fftn(fourier_first_order_density[i_channel] * fourier_wavelets[i_wavelet]))**2

    if return_square:
        return np.sqrt(second_order_density), second_order_density
    else:
        return np.sqrt(second_order_density)
