"""A collection of wavelets frequently used in our notebooks"""
import numpy as np
import numbers
from scipy.special import binom
from numpy.polynomial.hermite_e import hermeval


def check_grid(grid_or_shape):
    """Performs some verifications on the grid_or_shape variable passed
    as an argument.
    
    Parameters
    ==========
    
    grid_or_shape: ndarray or iterable
        specifies either the shape of a grid or is a grid array itself.
        A shape is an iterable of no more than 3 integer entries.
        A grid is an ndarray of dimension at most 4, where the first axis
        counts the number of dimensions

    Returns
    =======
    grid: ndarray."""

    grid_or_shape_ = np.atleast_1d(grid_or_shape)

    if grid_or_shape_.dtype == int:
        if grid_or_shape_.ndim <= 1:
            if len(grid_or_shape_) <= 3:
                # under these three conditions we interpret it as a shape
                shape = grid_or_shape_
                starts = -(shape // 2)
                stops = starts + shape
                slices = [slice(start, stop) for start, stop in
                        zip(starts, stops)]
                grid = np.mgrid[slices]
                return grid
    # Otherwise we interpret is a grid and do some dimensionality verifications
    grid = grid_or_shape_
    if grid.ndim > 4:
        raise ValueError("Grid dimension must not exceed 4")
    if grid.ndim > 1:
        if grid.shape[0] > 3:
            raise ValueError("For a more than 1D array grid, the first axis "
                            "size must correspond to the dimensionality of "
                            "the grid, which can only be 1, 2 or 3.")
    return grid



def get_2D_angles(grid):
    y, x = grid
    return np.arctan2(y, x)


def angular_harmonic(k, angles):
    # k_ = np.atleast_1d(k).ravel()
    harm = np.exp(1j * angles.T[..., np.newaxis] * k).T.copy()
    if isinstance(k, numbers.Number):
        return harm[0]
    return harm

from scipy.special import sph_harm   # import here to be next to angular_harmon

def get_3D_angles(grid):
    z, y, x = grid
    azimuthal = np.arctan2(y, x)
    rxy = np.sqrt(x ** 2 + y ** 2)
    polar = np.arctan2(z, rxy) + np.pi / 2
    return polar, azimuthal


def _s_gaussian_old(grid,sigmas):
    dimension=grid.shape[0]

    sigmas_ = np.atleast_1d(sigmas).ravel()
    # print(sigmas)
    radii_squared =(grid**2).sum(0)
    # sigmas_=np.array([1,2,3])

    # gaussians = np.zeros((dimension,)+grid.shape[1:])
    # gaussians = (np.exp(-.5 * radii_squared /
            # (sigmas_ ** 2)) / (np.sqrt(2 * np.pi *(sigmas_**2)) ** dimension))
    gaussians = (np.exp(-.5 * radii_squared[np.newaxis].T /
            (sigmas_ ** 2)) / (np.sqrt(2 * np.pi *(sigmas_**2)) ** dimension)).T
    return gaussians

def _s_gaussian(grid,sigmas):
    dimension=grid.shape[0]
    grid = grid.astype('float')
    sigmas_ = np.atleast_1d(sigmas).ravel().astype('float')
    # print(sigmas)
    radii_squared =(grid**2).sum(0)

    gaussians = np.zeros((sigmas_.shape[0],)+grid.shape[1:])
    # print(grid.shape,gaussians.shape)
    for i, sigma in enumerate(sigmas_):
        gaussians[i]=(np.exp(-radii_squared/(sigma**2)/2.) /  
            (np.sqrt(2 * np.pi *(sigma**2)) ** dimension))
    return gaussians

def _s_solid_harmonics(grid_or_shape, sigmas, n_oscillations=(0, 1, 2, 3, 4),
        ifftshift=True):
    grid = check_grid(grid_or_shape)
    # print(sigmas)
    # print(grid.max(),grid.min())

    dimension = grid.shape[0]
    sigmas_, n_oscillations_ = (np.atleast_1d(sigmas).ravel(),
                                np.atleast_1d(n_oscillations).ravel())
    radii_squared = (grid ** 2).sum(0)
    radii_power_l = (radii_squared[np.newaxis].T ** (n_oscillations_ / 2.)).T
    gaussians = _s_gaussian(grid,sigmas_)
    if dimension == 2:
        # create angular harmonics
        angles = get_2D_angles(grid)
        harmonics = angular_harmonic(n_oscillations_, angles) * radii_power_l
    elif dimension == 3:
        polar, azimuthal = get_3D_angles(grid)
        harmonics = []
        for l_ind, l in enumerate(n_oscillations):
            for m in range(-l, l + 1):
                harmonics.append(sph_harm(m, l, azimuthal, polar) *
                                    radii_power_l[l_ind])
        harmonics = np.array(harmonics)
    else:
        raise ValueError("Dimensionality must be 2 or 3, got {}".format(
                                                                    dimension))
    solid_harm = gaussians[:, np.newaxis] * harmonics[np.newaxis, :]
    if ifftshift:
        solid_harm = np.fft.ifftshift(solid_harm, axes=range(2, solid_harm.ndim))
    return solid_harm

def _f_gaussian(grid,sigmas):
    dimension=grid.shape[0]
    sigmas_ = np.atleast_1d(sigmas).ravel()
    radii_squared =(grid**2).sum(0)
    gaussians = np.zeros((sigmas_.shape[0],)+grid.shape[1:],dtype='complex128')
    for i,sigma in enumerate(sigmas_):
        gaussians[i] = (np.exp(-.5 * radii_squared *
            sigma ** 2) / np.sqrt(2 * np.pi  ) ** dimension)
        gaussians[i]=gaussians[i]*np.power(2*np.pi,3/2)

    return gaussians

def _get_Kl_normalization(n_oscillations_):
    def facodd(l):
        if l<1:
            return 1
        else:
            return np.prod(np.arange(l,0,-2))
    def fac(l): 
        if l<1:
            return 1
        else:
            return np.prod(np.arange(l,0,-1))
    Kl=np.zeros((n_oscillations_.shape[0]),dtype='complex128')
    for i,l in enumerate(n_oscillations_):
        if l%2==0:
            denom=np.power(2,(l-1)/2)*np.sqrt(2*l+1)*fac((l+1)/2)
            Kl[i]=np.pi/denom
        elif l%2==1:
            denom=np.sqrt(2*l+1)*facodd(l+1)
            Kl[i]=np.sqrt(8*np.pi)/denom
    return Kl

def _get_f_normalization(sigmas_,n_oscillations_):
    Kl = _get_Kl_normalization(n_oscillations_)
    numerator = (4*np.pi*(-(0+1j)*sigmas_[:,np.newaxis])**n_oscillations_[np.newaxis,:])
    numerator = numerator*Kl[np.newaxis,:]
    denom = np.power(2*np.pi,3./2)
    return numerator/denom

def _f_solid_harmonics(grid_or_shape, sigmas, n_oscillations=(0, 1, 2, 3, 4),
        ifftshift=True,tol=1e-5, normalized=True):
    grid = check_grid(grid_or_shape)

    if np.max(grid)>np.pi+tol or np.min(grid)<-np.pi-tol:
        raise Warning("grid dims our of [-pi,pi]")

    dimension = grid.shape[0]
    sigmas_, n_oscillations_ = (np.atleast_1d(sigmas).ravel(),
                                np.atleast_1d(n_oscillations).ravel())
    radii_squared = (grid ** 2).sum(0)
    radii_power_l = (np.sqrt(radii_squared[np.newaxis,np.newaxis].T) ** n_oscillations_ ).T
    gaussians = _f_gaussian(grid,sigmas_) 
    # gaussians
    # Kfactor=_get_Kl_normalization(n_oscillations_)
    # Kfactor*=(4*np.pi*(-(0+1j))**(n_oscillations_))/np.power(2*np.pi,3/2)
    solid_harm=None
    if dimension == 2:
        # create angular harmonics
        angles = get_2D_angles(grid)
        harmonics = angular_harmonic(n_oscillations_, angles) * radii_power_l
        solid_harm = gaussians[:, np.newaxis] * harmonics[np.newaxis, :]
        scaling=sigmas_[:,np.newaxis]**n_oscillations_[np.newaxis,:]
        solid_harm=scaling[:,:,np.newaxis,np.newaxis]*gaussians
    elif dimension == 3:
        scaling = _get_f_normalization(sigmas_,n_oscillations_)
        polar, azimuthal = get_3D_angles(grid)
        harmonics = []
        scaling=_get_f_normalization(sigmas_,n_oscillations_)
        for l_ind, l in enumerate(n_oscillations):
            for m in range(-l, l + 1):
                harm=(sph_harm(m, l, azimuthal, polar) * radii_power_l[l_ind])
                # scaling=((sigmas_**n_oscillations_[l_ind])*Kfactor[l_ind])
                harm=scaling[:,l_ind,np.newaxis,np.newaxis,np.newaxis]*harm
                harm*=gaussians
                harmonics.append(harm)
        # print(len(harmonics),harmonics[0].shape,np.array(harmonics).shape)
        solid_harm = np.array(harmonics).transpose((1,0,2,3,4))
    else:
        raise ValueError("Dimensionality must be 2 or 3, got {}".format(
                                                                    dimension))
    # solid_harm = gaussians[:, np.newaxis] * harmonics[np.newaxis, :]
    if ifftshift:
        solid_harm = np.fft.ifftshift(solid_harm, axes=range(2, solid_harm.ndim))
    return solid_harm

def lm2ind(l,m):
    return (2*np.arange(l)).sum()+1+m

def ind2lm(ind):
    if ind==0:
        return 0,0
    l=1
    lsum=(2*np.arange(l)).sum()+1
    while lsum<ind:
        l+=1
        lsum=(2*np.arange(l)).sum()+1
    m=np.arange(-l,l+1)[ind-lsum]
    return l,m


def solid_harmonics(grid_or_shape, sigmas, n_oscillations=(0, 1, 2, 3, 4),
        ifftshift=True,fourier=True):
    if fourier:
        return _f_solid_harmonics(grid_or_shape, sigmas, n_oscillations, ifftshift)
    else:
        return _s_solid_harmonics(grid_or_shape, sigmas, n_oscillations, ifftshift)

