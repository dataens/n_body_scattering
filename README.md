# README #

This repository holds the code used for the numerical experiments in 

[Solid Harmonic Wavelet Scattering for Predictions of Molecule Properties](https://arxiv.org/pdf/1805.00571.pdf)

[Solid Harmonic Wavelet Scattering: Predicting Quantum Molecular Energy from Invariant Descriptors of 3D Electronic Densities](https://papers.nips.cc/paper/7232-solid-harmonic-wavelet-scattering-predicting-quantum-molecular-energy-from-invariant-descriptors-of-3d-electronic-densities.pdf)

### What is this repository for? ###

* CPU and GPU implementation of scattering in 3D
* Implementation of 3D solid harmonic wavelets
* Version 0.0

### How do I get set up? ###

The code is built on top of pycuda, scikit-learn, and numpy. Additionally, you need to install the [CheML](https://github.com/CheML/CheML) package to obtain information about the molecules.

```
$ git clone https://github.com/CheML/CheML.git
$ cd CheML 
$ python setup.py install
$ pip install numpy
$ pip install pycuda
$ pip install  scikit-learn
```


### Installation ###

To install perform the following steps:

```
$ git clone https://<username>@bitbucket.org/dataens/n_body_scattering.git
$ cd n_body_scattering 
$ python setup.py install
```


### Contact ###

* Georgios Exarchakis (georgios.exarchakis@ens.fr), Michael Eickenberg (michael.eickenberg@gmail.com), Louis Thiry (louis.thiry@outlook.fr)
