from setuptools import setup

setup(name='n_body_scattering',
      version='0.0.1',
      description='Scattering tools for computational chemistry',
      url='https://gexarcha@bitbucket.org/dataens/n_body_scattering.git<Paste>',
      author='Georgios Exarchakis',
      author_email='georgios.exarchakis@ens.fr',
      license='BSD 3-clause',
      packages=['n_body_scattering'],    # understand what this means, sklearn-theano puts setuptools.find_packages()
      install_requires=['numpy',
                        'scipy',
                        'scikit-learn',
                        'pycuda'],
      classifiers=[ 'Development Status :: 3 - Alpha',
                    'Intended Audience :: Science/Research',
                    'License :: OSI Approved :: BSD License',
                    'Operating System :: OS Independent',
                    'Topic :: Scientific/Engineering']
      )



